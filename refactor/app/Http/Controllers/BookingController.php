<?php

namespace DTApi\Http\Controllers;

use DTApi\Models\Job;
use DTApi\Http\Requests;
use DTApi\Models\Distance;
use Illuminate\Http\Request;
use app\Repository\BookingRepository;
use Illuminate\Support\Facades\Auth;

/**
 * Class BookingController
 * @package DTApi\Http\Controllers
 */
class BookingController extends Controller
{

    protected $repository;
     protected $user;
    public function __construct(BookingRepository $bookingRepository,Request $request)
    {
        $this->repository = $bookingRepository;
        $user                       = Auth::user() ; // $request->__authenticatedUser;
    }

    public function index(Request $request)
    {
        $input                        = $request->all();
     
        if($this->user->id == $input['user_id']) {

            $response                 = $this->repository->getUsersJobs($this->user->id );

        }
        else if($this->user->user_type == env('ADMIN_ROLE_ID') || $this->user->user_type == env('SUPERADMIN_ROLE_ID'))
        {
            $response = $this->repository->getAll($request);
        }

        return response()->json($response);
    }

    public function show($id)
    {
        $job = $this->repository->find($id)->with('translatorJobRel.user');

        return response()->json($job);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $response = $this->repository->store($this->user, $data);

        return response()->json($response);

    }

    public function update($id, Request $request)
    {
        $data = $request->all();
        $cuser = $this->user;
        $response = $this->repository->updateJob($id, array_except($data, ['_token', 'submit']), $cuser);

        return response()->json($response);
    }


    public function immediateJobEmail(Request $request)
    {
        $adminSenderEmail = config('app.adminemail');
        $data = $request->all();

        $response = $this->repository->storeJobEmail($data);

        return response()->json($response);
    }

    public function getHistory(Request $request)
    {
        if($user_id = $request->user_id) {

            $response = $this->repository->getUsersJobsHistory($user_id, $request);
            return response()->json($response);
        }

        return null;
    }
    
    public function acceptJob(Request $request)
    {
        $data = $request->all();
        $user = $this->user;

        $response = $this->repository->acceptJob($data, $user);

        return response()->json($response);
    }

    public function acceptJobWithId(Request $request)
    {
        $data = $request->get('job_id');
        $user = $this->user;

        $response = $this->repository->acceptJobWithId($data, $user);

        return response()->json($response);
    }


    public function cancelJob(Request $request)
    {
        $data = $request->all();
        $user = $this->user;

        $response = $this->repository->cancelJobAjax($data, $user);

        return response()->json($response);
    }


    public function endJob(Request $request)
    {
        $data = $request->all();

        $response = $this->repository->endJob($data);

        return response()->json($response);

    }

    public function customerNotCall(Request $request)
    {
        $data = $request->all();

        $response = $this->repository->customerNotCall($data);

        return response()->json($response);

    }

    public function getPotentialJobs(Request $request)
    {
        $data = $request->all();
        $user = $this->user;

        $response = $this->repository->getPotentialJobs($user);

        return response()->json($response);
    }

    public function distanceFeed(Request $request)
    {
        $data = $request->all();

        if (isset($data['distance']) && $data['distance'] != null) {
            $distance = $data['distance'];
        } else {
            $distance = "";
        }
        if (isset($data['time']) && $data['time'] != null) {
            $time = $data['time'];
        } else {
            $time = "";
        }
        if (isset($data['jobid']) && $data['jobid'] != null) {
            $jobid = $data['jobid'];
        }

        if (isset($data['session_time']) && $data['session_time'] != null) {
            $session = $data['session_time'];
        } else {
            $session = "";
        }

        if ($data['flagged'] == 'true') {
            if($data['admincomment'] == null) return "Please, add comment";
            $flagged = 'yes';
        } else {
            $flagged = 'no';
        }
        
        if ($data['manually_handled'] == 'true') {
            $manually_handled = 'yes';
        } else {
            $manually_handled = 'no';
        }

        if ($data['by_admin'] == 'true') {
            $by_admin = 'yes';
        } else {
            $by_admin = 'no';
        }

        if (isset($data['admincomment']) && $data['admincomment'] != null) {
            $admincomment = $data['admincomment'];
        } else {
            $admincomment = "";
        }
        if ($time || $distance) {

            $affectedRows = Distance::where('job_id', '=', $jobid)->update(array('distance' => $distance, 'time' => $time));
        }

        if ($admincomment || $session || $flagged || $manually_handled || $by_admin) {

            $affectedRows1 = Job::where('id', '=', $jobid)->update(array('admin_comments' => $admincomment, 'flagged' => $flagged, 'session_time' => $session, 'manually_handled' => $manually_handled, 'by_admin' => $by_admin));

        }

        return response('Record updated!');
    }

    public function reopen(Request $request)
    {
        $data = $request->all();
        $response = $this->repository->reopen($data);

        return response()->json($response);
    }

    public function resendNotifications(Request $request)
    {
        $data = $request->all();
        $job = $this->repository->find($data['jobid']);
        $job_data = $this->repository->jobToData($job);
        $this->repository->sendNotificationTranslator($job, $job_data, '*');

        return response(['success' => 'Push sent']);
    }


    public function resendSMSNotifications(Request $request)
    {
        $data = $request->all();
        $job = $this->repository->find($data['jobid']);
        $job_data = $this->repository->jobToData($job);

        try {
            $this->repository->sendSMSNotificationToTranslator($job);
            return response()->json(['success' => 'SMS sent']);
        } catch (\Exception $e) {
            return response()->json(['success' => $e->getMessage()]);
        }
    }

}
